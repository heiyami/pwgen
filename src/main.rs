extern crate argparse;

use std::io::{BufRead};

use argparse::{ArgumentParser, Store, StoreTrue};
use rand::seq::SliceRandom;

// Constant(s) =====================================================================================

// Is there a pre-defined set of these?
const ALPHA_LOWER_POOL: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
];

const ALPHA_UPPER_POOL: [char; 26] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

const DIGIT_POOL: [char; 10] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

const SYMBOL_POOL: [char; 32] = [
    '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=',
    '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~',
];

// Helper enum(s) ==================================================================================

enum CharType {
    AlphaLower,
    AlphaUpper,
    Digit,
    Symbol,
}

// Helper struct(s) ================================================================================

struct Options {
    length: usize,
    has_alpha_upper: bool,
    has_digit: bool,
    has_symbol: bool,
}

// Helper function(s) ==============================================================================

fn read_args() -> Options {
    let mut o_length: usize = 12;
    let mut o_has_alpha_upper: bool = false;
    let mut o_has_digit: bool = false;
    let mut o_has_symbol: bool = false;

    {
        let mut ap = ArgumentParser::new();
        ap.set_description("A simple command-line password generator in Rust.");

        ap.refer(&mut o_length).add_argument(
            "length",
            Store,
            "The length of the generated password.",
        );

        ap.refer(&mut o_has_alpha_upper).add_option(
            &["-u", "--upper"],
            StoreTrue,
            "Include at least one upper case letter in the password.",
        );

        ap.refer(&mut o_has_digit).add_option(
            &["-d", "--digit"],
            StoreTrue,
            "Include at least one digit in the password.",
        );

        ap.refer(&mut o_has_symbol).add_option(
            &["-s", "--symbol"],
            StoreTrue,
            "Include at least one special character in the password.",
        );

        ap.parse_args_or_exit();
    }

    Options {
        length: o_length,
        has_alpha_upper: o_has_alpha_upper,
        has_digit: o_has_digit,
        has_symbol: o_has_symbol,
    }
}

fn get_char_types(_options: &Options) -> Vec<&'static CharType> {
    let mut valid_types: Vec<&'static CharType> = vec![&CharType::AlphaLower];

    if _options.has_alpha_upper {
        valid_types.push(&CharType::AlphaUpper);
    }

    if _options.has_digit {
        valid_types.push(&CharType::Digit);
    }

    if _options.has_symbol {
        valid_types.push(&CharType::Symbol);
    }

    valid_types
}

fn get_random_char_type(valid_types: &Vec<&'static CharType>) -> &'static CharType {
    let mut rng = rand::thread_rng();
    valid_types.choose(&mut rng).unwrap()
}

fn get_random_char(char_type: &'static CharType) -> &char {
    match char_type {
        &CharType::AlphaLower => ALPHA_LOWER_POOL.choose(&mut rand::thread_rng()).unwrap(),
        &CharType::AlphaUpper => ALPHA_UPPER_POOL.choose(&mut rand::thread_rng()).unwrap(),
        &CharType::Digit => DIGIT_POOL.choose(&mut rand::thread_rng()).unwrap(),
        &CharType::Symbol => SYMBOL_POOL.choose(&mut rand::thread_rng()).unwrap(),
    }
}

fn generate_password(_options: &Options, pw_valid_types: &Vec<&'static CharType>) {
    let mut pw_chars: Vec<&char> = Vec::with_capacity(_options.length);

    while pw_chars.len() < pw_chars.capacity() {
        let rand_type = get_random_char_type(pw_valid_types);
        pw_chars.push(get_random_char(rand_type));
    }

    pw_chars.shuffle(&mut rand::thread_rng());

    let password: String = pw_chars.into_iter().collect();
    println!("\n{}", password);
}

// Main method =====================================================================================

fn main() {
    // For reading user input.
    let mut buffer = String::new();
    let stdin = std::io::stdin();

    // Read command-line arguments.
    let options: Options = read_args();

    // Generate password in a loop.
    let mut stop: bool = false;
    let pw_valid_types: Vec<&'static CharType> = get_char_types(&options);

    while !stop {
        buffer.clear();
        generate_password(&options, &pw_valid_types);
        println!("Generate another password? [y/n]");
        stdin.lock().read_line(&mut buffer).expect("Could not read line.");
        stop = buffer.trim() == "n";
    }
}
